load "Revlog.rb"
class Filelog < Revlog
	attr_accessor :name, :file, :repo_dir, :log, :filename
  #initialization of File log with duplicate check
  def initialize(filename)
  	@filename = filename
    @name = File.basename(filename)
    @repo_dir = File.dirname(filename)
    @log = File.new(@repo_dir+'/objects/'+@name+'.log', 'w+')\
      if File.exist?(@repo_dir+'/objects/'+@name+'.log') == false
  end
  
  #return the the content into a string
  def get()
    return IO.read(@repo_dir+'/objects/'+@name+'.log')
  end

  def getDiff(sha)
    @log.split("\0\0").each {|l| if l.split("\0")[0] == sha then
      return l.split("\0")[1]
    end}
  end

    
 
  #add changes to filelog
  def add_change()
    diff = self.diff(self.rev(self.tip), IO.read(@filename))
  	sha = add(IO.read(@filename))
    @log.write(sha+"\0"+diff+"\0\0")
  	text = ''
  	#should be IO instead of File
  	IO.read(@repo_dir+'/index').split("\0\0").each {|l| if l.split("\0")[0] == @name then
  		#l.split('\0')[1] = sha
  		text += @name+"\0"+sha+"\0\0"
  	else
  		text += l+"\0\0"
  	end
  	}
  	File.open(@repo_dir+'/index', "w") {|file| file.puts text }
  	return sha
  end
end
