load "Revlog.rb"
class Mchangelog < Revlog
  attr_accessor :filelogs
  def initialize
    #mani returns the information and list of changed file given the SHA of commit
    @@mani = Hash.new()
    #diffs return the content of the changes of files, given the SHA and the name
    @@diffs = Hash.new()
    #newest note the latest SHA of each file
    @@newest = Hash.new()
    #filelogs store (name, filelog) tuples. Returns filelog given name
    @filelogs = Hash.new()
  end
  
  def info (sha, message="")
    r = ""
    r += $userinfo
    r += Time.now.to_s
    r += "\0"
    r += message
    r += "\0"
    r += sha
    r += "\0\0"
    return r
  end
  
  def manifest (sha, list, message="")
    @@mani[sha] = info(sha, message)
    list.each do |i|
      @@mani[sha] += i
      @@mani[sha] += "\t"
      if @filelogs.has_key?(i) then
      f = @filelogs[i]
    else
      f = Filelog.new(i)
    end
      fileSHA = f.tip
      @@mani[sha] += fileSHA
      @@mani[sha] += "\0"
    end
  end
  
  def getmani (a)
    return @@mani[a]
  end
  
  #return the newest SHA of the file
  def getchange(name)
    return @@newest[name]
  end
  
  #give the SHA and the file name, return the changed content
  def getcontent(commit_sha, name)
    return @@diff[commit_sha[name]]
  end
  
  #modified the add function, with list of changes and sha of commit using a nested hash
  def add (list, message="")
    commitSHA = Util.hash(list.join("\0"))
    list.each do |i|
      oldSHA = getchange(i)
      if @filelogs.has_key?(i) then
      f = @filelogs[i]
    else
      f = Filelog.new(i)
    end
      newSHA = f.tip
      #suppose diff function is from Revlog and take 2 shars as input return a string
      str = Revlog.diff(oldSHA, newSHA)
      @@diff[newSHA[i]] = str
      @@newest[i] = newSHA
    end
    self.manifest(commitSHA, list, message)
  end

  def switchRev(sha)
    manifest_text = @@mani[sha]
    (manifest_text.split("\0\0")[1]).split("\0").each{|f| 
      filelog = @filelogs[f.split("\t")[0]]
      file = File.open(filelog.filename, 'w') {|file| file.truncate(0) }
      file.write(filelog.rev(f.split("\t")[1], 'w+')
    }

end
