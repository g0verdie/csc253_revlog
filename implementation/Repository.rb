load 'Revlog.rb'
load 'Filelog.rb'
load 'Mchangelog.rb'

class Repository

	attr_accessor :name, :email, :path, :git_path, :cur_branch, :manifest, :head

	# Intialize the Repository, user needs to enter the name and email address
	# if the specific path is not given, then create at the current directory
	def initialize(path=Dir.pwd, usr, email)
		# Name with "git" for now
		if !Dir.exist?(path+'/.git') then
		Dir.mkdir(path + '/.git')
		@name = usr
		@email = email
		#project location
		@path = path
		#location of the version control repo structure
		@git_path = @path + '/.git'
		@cur_branch = "master"
		@manifest = Mchangelog.new

		Dir.mkdir(@git_path + '/branch')
		Dir.mkdir(@git_path + '/refs')
		Dir.mkdir(@git_path + '/objects')
		File.new(@git_path + '/branch' +'/master', 'w+')
		File.new(@git_path + '/index', 'w+')
		File.new(@git_path + '/stage', 'w+')
		File.new(@git_path + '/head', 'w+')
		# head at beginning will be 0
		File.write(@git_path + '/head', '0')
	end
	end

	def push

	end

	def pull

	end

	def root
		self.git_path
	end

	def add(names)
		# give list of files' names to Revlog? or content? 
		# return sha of each file?
		# names = ["a.txt", "b.txt", "c.txt"]
		changed_files = Array.new
		names.each do |name|
			if manifest.filelogs.has_key?(name) then
				f = manifest.filelogs[name]
			else
			f = Filelog.new(self.path+"/#{name}")
		end
			content = f.rev(f.tip)
			
			if content != File.read(self.path+"/#{name}") then
				changed_files << (name)
				f.add_change
			end
			#files_paths << self.path + "/#{name}"
		end

		# files_paths = ["xxx/a.txt", "xxx/b.txt", "xxx/c.txt"]
		#Revlog.add(files_paths)
		# and in stage file
		# a.txt Sha1
		# b.txt Sha2
		File.open(@git_path + '/stage', 'w+') { |file| file.write(@head+"\0") }
		changed_files.each{|name| File.open(@git_path + '/stage', 'w+') { |file| file.write(name+"\0") }}
		File.open(@git_path + '/stage', 'w+') { |file| file.write("\0\0") }
	end

	# in branch folder
	# different file represents different branches with different branch name
	# 
	def branch(name)
		# change current branch
		self.cur_branch = name
		# create a new file under branch folder with the name 
		f = File.new(self.root + '/branch' + "/#{self.cur_branch}", 'w+')
	end

	# give message and list of changed files in stage file to Manifest
	def commit(message)
		
		changed_files = Array.new

		File.open(@git_path + '/stage', 'r').to_s.split("\0\0").each{|l|
			if l.split("\0")[0] == @head then
				new_sha = @manifest.add(message, @name, l.split("\0")[1:])
			end
			} 

		# add this commit to the current branch
		File.open(self.git_path + '/branch' + "/#{self.cur_branch}", 'a') { |f| f.write("#{new_sha}\n")}
		# update head
		File.write(self.git_path + '/head', "#{new_sha}")
		File.open(self.git_path + '/stage', 'w') {|file| file.truncate(0) }
		@head = new_sha
		return new_sha
	end

	# to a different branch, or a specific state of working directory
	def checkout(target)
		# if it only contains alphabet, which means it's a branch name
		# get last commit made on the branch
		if target[/[a-zA-Z]+/] == target
			self.cur_branch = target
			current_commit = File.open(self.git_path + '/branch' + "/#{target}").to_a.last
		else
			# or changing working directory, get certain commit
			current_commit = target
		end
		# update head
		File.write(self.git_path + '/head', "#{current_commit}")
		@manifest.switchRev(current_commit)

	end

end

