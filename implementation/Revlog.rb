# Revlog implementation
require 'digest'
require 'diffy'

class Revlog
    # Creates a new Revlog instance
    # @path: the path to the repository data directory (".git" equivalent)
    # @hash: the hash of the tip of the revision log, or nil to create a new revision log
    def initialize(path, hash = nil)
        @path = path
        @hash_rev_map = {}
        if hash.nil? then
            @tip = ''
        else
            @tip = hash
            ancestors = [@tip]
            ancestors.each do |hash|
                rev = Revision.new(@path, hash)
                @hash_rev_map[hash] = rev
                ancestors += rev.parents
            end
        end
    end

    # Gets the hash of the latest
    # @return: the hash of the latest revision, or the empty string if this is a new revision log (no entries yet)
    def tip
        @tip
    end

    # Gets the hashes of the parents of a revision
    # @hash: the hash of the revision whose parents to get
    # @return: an array containing the hashes of the parents of the revision, may be empty
    def parents(hash)
        raise "Hash not present in revision log" unless @hash_rev_map.key? hash
        @hash_rev_map[hash].parents
    end

    # Gets the contents of a revision
    # @hash: the hash of the revision whose contents to get
    # @return: the contents of the revision
    def rev(hash)
        raise "Hash not present in revision log" unless @hash_rev_map.key? hash
        @hash_rev_map[hash].rev
    end

    # Adds a new revision with this as its parent
    # @contents: the contents of the new revision
    # @return: the hash of the new revision
    def add(contents)
        type = Revision.get_type(tip.empty?, false, false)
        parent = unless tip.empty? then Util.to_bin(tip) else '' end

        #TODO: diffs
        data = type + parent + Util.compress(contents)
        hash = Util.from_bin(Util.hash(data))
        hash =~ Util.hash_split
        dir = $1
        file = $2
        file = File.join(@path, Util.object_dir, dir, file)

        Util.ensure_path(file)
        raise "Hash collision" if File.file?(file)
        File.open(file, 'w') do |f|
            raise "Failure writing new object file" unless f.write(data) == data.length
        end

        @hash_rev_map[hash] = Revision.new(@path, hash)
        @tip = hash
        hash
    end

    # Merges this revision with another
    # @other: the hash of the revision to merge with
    # @return: the hash of the merge
    def merge(other)
        #TODO: implement this
        "0" * 128
    end

    # Returns the difference between revisions using Diffy gem
    # @sha1: the hash of the first revision
    # @sha2: the has of the second revision
    # @return: diff string
    def diff(sha1, sha2)
        return Diffy::Diff.new(self.rev(sha1), self.rev(sha2))
    end

    # Returns the difference between revision and provided contents
    # @sha: the sha of the revision
    # @contents: the contents revision is being diffed against.
    # @return: diff strings
    def diffSHAStr(sha, contents)
        return Diffy::Diff.new(self.rev(sha), contents)
    end
end

class Revision
    # The "root bit"
    # 0 for the root object (no parent), 1 otherwise
    @@root_bit = 1

    # The "merge bit"
    # 0 for single parent, 1 for two parents (merge)
    # It is never valid for this to be set if the root bit is also set
    @@merge_bit = 2

    # The "diff bit"
    # 0 if this object contains the full contents, 1 for a diff
    # It is never valid for this to be set if the root bit is also set
    @@diff_bit = 4

    # Creates a new Revision instance
    # @path: the path to the repository data directory (".git" equivalent)
    # @hash: the hash of the revision
    def initialize(path, hash)
        @path = path
        raise "Invalid hash" unless hash =~ Util.hash_split
        @tip = hash
        @dir = $1
        @file = $2
        File.open(File.join(path, Util.object_dir, @dir, @file)) do |f|
            type = f.readbyte
            if type & @@root_bit != 0 then @root = true else @root = false end
            if type & @@merge_bit != 0 then @merge = true else @merge = false end
            if type & @@diff_bit != 0 then @diff = true else @diff = false end
            unless @root then
                parent = f.read(64)
                raise "Invalid object file" unless parent.length == 64
                @parents = [Util.from_bin(parent)]
            else
                @parents = []
            end
            if @merge then
                parent = f.read(64)
                raise "Invalid object file" unless parent.length == 64
                @parents << Util.from_bin(parent)
            end
            if @diff then
                # TODO: implement diffs
            else
                @contents = Util.uncompress(f.read)
            end
        end
    end

    # Gets the hash of this revision
    # @return: the hash of this revision, or the empty string if this is a new revision log (no entries yet)
    def tip
        @tip
    end

    # Gets the hashes of the parents of this revision
    # @return: an array containing the hashes of the parents of this revision, may be empty
    def parents
        @parents
    end

    # Gets the contents of this revision
    # @return: the contents of this revision
    def rev
        @contents
    end

    # Gets the type byte, as a byte buffer, for the given type
    # @root: boolean value representing if the type is a root
    # @merge: boolean value representing if the type is a merge
    # @diff: boolean value representing if the type is a diff
    # @return: the byte buffer for the specified type
    def self.get_type(root, merge, diff)
        type = 0
        if root then type += @@root_bit end
        if merge then type += @@merge_bit end
        if diff then type += @@diff_bit end
        [type].pack("C")
    end
end

class Util
    # Compresses data
    # @data: the data to compress
    # @return: the compressed data
    def self.compress(data)
        #TODO: actual compression
        data
    end

    # Uncompresses data
    # @data: the data to uncompress
    # @return: the uncompressed data
    def self.uncompress(data)
        #TODO: actual compression
        data
    end

    # Ensures that the dirname of a file exists
    # @file: the file whose dirname must exist
    def self.ensure_path(file)
        dirname = File.dirname(file)
        unless File.directory?(dirname)
            ensure_path(dirname)
            Dir.mkdir(dirname)
        end
    end

    # Converts a byte buffer to a hex string
    # @binstr: the byte buffer
    # @return: the hex string representation of @binstr
    def self.from_bin(binstr)
        binstr.unpack("H*")[0]
    end

    # Converts a hex string to a byte buffer
    # @hexstr: the hex string
    # @return: the byte buffer corresponding to @hexstr
    def self.to_bin(hexstr)
        [hexstr].pack("H*")
    end

    # Calculates the hash of a string
    # @message: the string of which to calculate the hash
    # @return: the hash
    def self.hash(message)
        Digest::SHA512.digest message
    end

    # Regex to split a hash into dir, file components
    def self.hash_split
        /([0-9a-f]{2})([0-9a-f]{62})/
    end
    
    # Subdirectory that contains the objects
    def self.object_dir
        'objects'
    end
end
