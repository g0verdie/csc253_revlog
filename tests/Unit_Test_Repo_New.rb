require_relative 'Repository'
require 'test/unit'
require 'fileutils'


class TestRepo < Test::Unit::TestCase

	def test_init
		path = "/Users/haochen/Desktop/Repo"
		repo = Repository.new(path, "tester", "tester@test.com")
		assert_equal("tester", repo.name)
		assert_equal("tester@test.com", repo.email)
		assert_equal(true, File.directory?('.git'))
		assert_equal(true, File.directory?('.git/branch'))
		assert_equal(true, File.directory?('.git/refs'))
		assert_equal(true, File.directory?('.git/objects'))
		assert_equal(true, File.exist?('.git/index'))
		assert_equal(true, File.exist?('.git/stage'))
		assert_equal(true, File.exist?('.git/head'))

		FileUtils.rm_rf(path + '/.git')

	end

	def test_root
		path = "/Users/haochen/Desktop/Repo"
		repo = Repository.new(path, "tester", "tester@test.com")
		assert_equal(repo.root, "/Users/haochen/Desktop/Repo/.git")

		FileUtils.rm_rf(path + '/.git')
	end

	def test_branch
		path = "/Users/haochen/Desktop/Repo"
		repo = Repository.new(path, "tester", "tester@test.com")
		repo.branch("test")
		#assert_equal(File.read(repo.root + '/branch' + '/test'), "0")

		#FileUtils.rm_rf(path + '/.git')
	end

end
