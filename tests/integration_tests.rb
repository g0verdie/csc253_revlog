require 'test/unit'
require 'revlog'
require 'mchangelog'
require 'filelog'
require 'repository'
class IntegrationTests < Test::Unit::TestCase

	def test_repo_revlog
		f = File.new('hello world.txt', 'w+')
		f.write("hello\n")
		s = File.new('foobar.txt', 'w+')
		s.write("foo\n")
		t = File.new('goodbye.txt', 'w+')
		t.write("goodbye!\n")

		r = Repository.new("csc253", "csc253@u.rochester.edu")
		r.add(f)
		sha = r.commit("first")
		r.add(s)
		sha2 = r.commit("second")
		r.add(t)
		sha3 = r.commit("third")

		assert_equal(sha, r.head, "Mismatched SHA tip")
		assert_equal("hello\n", (r.manifest.filelogs[f.basename]).rev(sha), "Mismatched revision info")
		assert_equal(sha, r.manifest.ancestor(sha2,sha3), "Mismatched ancestor")
		p "#{__method__} succeed\nrepository revlog integration successful"
	end

	def test_change_tracking
		f = File.new('hello world.txt', 'w+')
		f.write("hello\n")
		r = Repository.new("csc253", "csc253@u.rochester.edu")
		r.add(f)
		r.commit("first")
		oldSha = r.manifest.getchange(f.basename)
		f.write("world\n")
		r.add(f)
		r.commit("second")

		assert_not_same(oldSha, r.manifest.getchange(f.basename), "File hasn't been updated")
	end

	def test_change_branch
		f = File.new('hello world.txt', 'w+')
		f.write("hello\n")
		r = Repository.new("csc253", "csc253@u.rochester.edu")
		r.add(f)
		oldBranchSha = r.commit("first")
		r.branch("newBranch")
		f.write("world\n")
		r.add(f)
		newBranchSha = r.commit("second")
		r.checkout("master")
		assert_same(oldBranchSha, r.head, "Unsuccessfully switched branches")
	end


	def test_repo_machangelog_filelog
		f = File.new('foo.txt', 'w+')
		f.write("hello\n")

		r = Repository.new("csc253", "csc253@u.rochester.edu")
		r.add(f)
		sha = r.commit("foo\n")
		r.add(s)
		sha2 = r.commit("bar\n")
		r.add(t)
		sha3 = r.commit("test\n")

		assert_equal(true, r.manifest.getmani(sha).include?("csc253"), "Mismatched Commit message")
		assert_equal(true, r.manifest.getmani(sha3).include?("foo.txt"), "Mismatched list of changed files")
		assert_equal("+test\n", r.manifest.getcontent(sha3, "foo.txt"), "mismatched diff")
		p "#{__method__} succeed"
	end
end