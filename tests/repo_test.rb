require 'Repository'
require 'test/unit'
require 'digest/sha1'

class TestRepo < Test::Unit::TestCase

	def setup
		@path = 'foo\dir' # directory to intialize a repo
		@repoPath = 'foo\dir\.git'
		@dest = 'foo\dest' # destination directory
		@source = 'foo\source' # source directory
		@file = 'foo\file' # list of files
		@sha = ''
	end

	def test_init
		begin
			Repository.new(@path) # should be the root
		rescue
			raise "Error: Has problem with intializing repo"
		end
		assert_equal(true, File.directory?(@repoPath))
	end

	def test_push
		begin
			Repository.push(@dest)
		rescue
			raise "Error: Has problem with push"
		end

	end

	def test_pull
		begin
			Repository.pull(@source)
		rescue
			raise "Error: Has problem with pushing"
		end
	end

	def test_root
		assert_equal(@path, Repository.root(), "Error: Not the correct root")
	end

	def test_branch
		begin
			Repository.branch("newBranch")
		rescue
			raise "Error: Has problem with creating a new branch"
		end
	end

	def test_add
		begin
			Repository.add(@file)
		rescue
			raise "Error: Has problem with adding files"
		end
	end

	def test_commit
		begin 
			Repository.commit("testing")
		rescue
			raise "Error: Has problem with committing"
		end
		File.write('test1.txt', '1')
		File.write('test2.txt', '2')
		commitSHA = Repository.commit("testing")
		assert_equal(commitSHA, [Digest::SHA1.hexdigest '1', Digest::SHA1.hexdigest '2'], "Error with commit")
	end

	def test_rm
		begin
			Repository.rm(test1.txt, test2.txt)
		rescue
			raise "Error: Has problem with rm"
		end
	end

	def test_checkout
		File.write('test1.txt', '1')
		checkoutSHA = Digest::SHA1.hexdigest '1'
		begin
			Repository.checkout(checkoutSHA)
		rescue
			raise "Error: Has problem with checkout"
		end
	end

end