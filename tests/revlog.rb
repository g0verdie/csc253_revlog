require 'test/unit'
require 'Revlog'

class TestRevlog < Test::Unit::TestCase
    def test_simple
        r = Revlog.new 't1'
        sha = r.add('foo')
        r = Revlog.new('t1', sha)
        assert_equal('foo', r.rev(sha), "Failed extracting data")
    end

    def test_multiple
        r = Revlog.new 't2'
        r.add('foo\nbar\nbaz')
        sha = r.add('foo\nbar\nbaz\n')
        r = Revlog.new('t2', sha)
        assert_equal('foo\nbar\nbaz\n', r.rev(sha), "Failed extracting data")
    end

    def test_parent
        r = Revlog.new 't3'
        parent_sha = r.add('foo\nbar\n')
        child_sha = r.add('foo\nbar\nbaz\n')
        assert_equal(parent_sha, r.parents(child_sha)[0], "Wrong parent")
    end

#    def test_merge
#        r = Revlog.new 't4'
#        parent_sha = r.add('foo\nbar\n')
#        r.add('foo\nbaz\nbar\n')
#        child2_sha = Revlog.new('t4', parent_sha).add('foo\nbar\nbaz\n')
#        merge_sha = r.merge(child2_sha)
#        assert_equal('foo\nbaz\nbar\nbaz\n', r.rev(merge_sha), "Merge failed")
#    end
#
#    def test_multiple_parents
#        r = Revlog.new 't5'
#        parent_sha = r.add('foo\nbar\n')
#        child1_sha = r.add('foo\nbaz\nbar\n')
#        child2_sha = Revlog.new('t5', parent_sha).add('foo\nbar\nbaz\n')
#        merge_sha = r.merge(child2_sha)
#        assert(r.parents(merge_sha) == [child1_sha, child2_sha] || r.parents(merge_sha) == [child2_sha, child1_sha], "Merge failed")
#    end
end
