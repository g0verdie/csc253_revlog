load 'implementation/Filelog.rb'
require 'test/unit'
class TestFilelog < Test::Unit::TestCase
  
  def test_init
    r = Filelog.new("tests/yolo")
    assert_equal(nil, r, "file doesn't exist")
    r = Filelog.new("tests/mchangelog_test.rb")
    assert_not_equal(r, nil, "initiation failed")
  end
end
t = TestFilelog.new(0)
t.test_init()