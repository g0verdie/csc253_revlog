load "implementation/Mchangelog.rb"
require 'test/unit'

class TestMchangelog < Test::Unit::TestCase


	def test_info
		rm = Mchangelog.new
		r = Revlog.new
		sha = r.add('foo')
		msg = rm.info(sha)
    assert_equal(msg,"UR Hacker\nCS@rochester.edu\n"+Time.now.to_s + "\nfoo\n","info is incorrect")
	end

	def test_add
		r = Mchangelog.new
		a=[]
		a[0] = "a.txt"
		a[1] = "b.txt"
		sha = r.add("yolo", a, ["+haha\n", "+world\n-hello\n"])
		a.each do |i|
		  print r.getchange(i)
		end
		a.each do |i|
		  print r.getmani(i)
		end
		
		assert_equal(sha,"foo", "add method fails")
	end
end
a = TestMchangelog.new(0)